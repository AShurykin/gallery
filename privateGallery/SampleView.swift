import UIKit

class SampleView: UIView {
    
    @IBOutlet weak var additionalView: UIView!
    @IBOutlet weak var mainTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    
    static func instanceFromNib() -> SampleView {
        return UINib(nibName: "SampleView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? SampleView ?? SampleView()
    }
}
