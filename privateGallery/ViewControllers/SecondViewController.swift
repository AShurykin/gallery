import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var galleryImageView: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var mainTextField: UITextField!
    @IBOutlet var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var leftGalleryImageViewConstraint: NSLayoutConstraint!
    @IBOutlet var rightGalleryImageViewConstraint: NSLayoutConstraint!
    
    var photoObjectsArray: [Photo] = []
    private var arrayForSave: [ImageAttributes] = []
    var imageObject: Photo?
    private var myImagesCounter = 0
    private lazy var leftX = galleryImageView.frame.origin.x - galleryImageView.frame.size.width
    private lazy var rightX = galleryImageView.frame.origin.x + galleryImageView.frame.size.width
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print(self,"deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.setTitle(backButtonTitle, for: .normal)
        guard let imageObject = imageObject else {
            return
        }
        mainTextField.delegate = self
        galleryImageView.image = imageObject.photo
        mainTextField.text = imageObject.comment
        if imageObject.likeState {
            likeButton.setBackgroundImage(UIImage(named: "heartFill"), for: .normal)
        } else {
            likeButton.setBackgroundImage(UIImage(named: "heart"), for: .normal)
        }
        setMyImagesCounterPosition()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(_sender:)))
        self.view.addGestureRecognizer(recognizer)
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        leftSwipeRecognizer.direction = .left
        self.view.addGestureRecognizer(leftSwipeRecognizer)
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registrationForKeyboartNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for item in photoObjectsArray {
            let itemForSave = ImageAttributes(imageFileURLName: item.name, likeState: item.likeState, comment: item.comment)
            arrayForSave.append(itemForSave)
        }
        UserDefaults.standard.set(encodable: arrayForSave, forKey: Keys.imageAttributes.rawValue)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        if photoObjectsArray[myImagesCounter].likeState {
            likeButton.setBackgroundImage(UIImage(named: "heart"), for: .normal)
            photoObjectsArray[myImagesCounter].likeState = false
        } else if photoObjectsArray[myImagesCounter].likeState == false{
            likeButton.setBackgroundImage(UIImage(named: "heartFill"), for: .normal)
            photoObjectsArray[myImagesCounter].likeState = true
        }
    }
    
    
    @objc private func dismissKeyboard(_sender: UITapGestureRecognizer) {
        view.endEditing(true)
        scrollViewBottomConstraint.constant = 50
        view.layoutIfNeeded()
    }
    
    @objc private func swipeDetected(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .right:
            addLeftImageView { (leftImageView) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.leftGalleryImageViewConstraint.constant += self.galleryImageView.frame.size.width
                    self.rightGalleryImageViewConstraint.constant += self.galleryImageView.frame.size.width
                    leftImageView.frame.origin.x += self.galleryImageView.frame.size.width
                    self.view.layoutIfNeeded()
                }) { (_) in
                    if self.photoObjectsArray[self.myImagesCounter].likeState {
                        self.likeButton.setBackgroundImage(UIImage(named: "heartFill"), for: .normal)
                    } else {
                        self.likeButton.setBackgroundImage(UIImage(named: "heart"), for: .normal)
                    }
                    self.mainTextField.text = self.photoObjectsArray[self.myImagesCounter].comment
                    self.galleryImageView.image = leftImageView.image
                    self.rightGalleryImageViewConstraint.constant -= self.galleryImageView.frame.size.width
                    self.leftGalleryImageViewConstraint.constant -= self.galleryImageView.frame.size.width
                    leftImageView.removeFromSuperview()
                }
            }
        case .left:
            addRightImageView { (rightImageView) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.leftGalleryImageViewConstraint.constant -= self.galleryImageView.frame.size.width
                    self.rightGalleryImageViewConstraint.constant -= self.galleryImageView.frame.size.width
                    rightImageView.frame.origin.x -= self.galleryImageView.frame.size.width
                    self.view.layoutIfNeeded()
                }) { (_) in
                    if self.photoObjectsArray[self.myImagesCounter].likeState {
                        self.likeButton.setBackgroundImage(UIImage(named: "heartFill"), for: .normal)
                    } else {
                        self.likeButton.setBackgroundImage(UIImage(named: "heart"), for: .normal)
                    }
                    self.mainTextField.text = self.photoObjectsArray[self.myImagesCounter].comment
                    self.galleryImageView.image = rightImageView.image
                    self.rightGalleryImageViewConstraint.constant += self.galleryImageView.frame.size.width
                    self.leftGalleryImageViewConstraint.constant += self.galleryImageView.frame.size.width
                    rightImageView.removeFromSuperview()
                }
            }
        default:
            break
        }
    }
    
    private func addLeftImageView(complition: @escaping (UIImageView) -> ()) {
        let leftImageView = UIImageView(frame: CGRect(
        x: leftX,
        y: galleryImageView.frame.origin.y,
        width: galleryImageView.frame.size.width,
        height: galleryImageView.frame.size.height))
        leftImageView.contentMode = .scaleAspectFit
        self.view.addSubview(leftImageView)
        myImagesCounter -= 1
        if myImagesCounter < 0 {
            myImagesCounter = photoObjectsArray.count - 1
            leftImageView.image = photoObjectsArray[myImagesCounter].photo
        } else {
            leftImageView.image = photoObjectsArray[myImagesCounter].photo
        }
        complition(leftImageView)
    }
    
    private func addRightImageView(complition: @escaping(UIImageView) -> ()) {
        let rightImageView = UIImageView(frame: CGRect(
            x: rightX,
            y: galleryImageView.frame.origin.y,
            width: galleryImageView.frame.size.width,
            height: galleryImageView.frame.size.height))
        rightImageView.contentMode = .scaleAspectFit
        self.view.addSubview(rightImageView)
        myImagesCounter += 1
        if myImagesCounter > photoObjectsArray.count - 1 {
            myImagesCounter = 0
            rightImageView.image = photoObjectsArray[myImagesCounter].photo
        } else {
            rightImageView.image = photoObjectsArray[myImagesCounter].photo
        }
        complition(rightImageView)
    }
    
    private func registrationForKeyboartNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeState(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeState(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillChangeState(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else {
            return
        }
        guard let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        if notification.name == UIResponder.keyboardWillShowNotification {
            scrollViewBottomConstraint.constant = keyboardScreenEndFrame.height
        } else {
            scrollViewBottomConstraint.constant = 0
        }
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func setMyImagesCounterPosition() {
        guard let photo = imageObject else {
            return
        }
        var counter = 0
        for item in photoObjectsArray {
            if item.name == photo.name {
                myImagesCounter = counter
            }
            counter += 1
        }
    }
}

extension SecondViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else {
            return false
        }
        photoObjectsArray[myImagesCounter].comment = text
        mainTextField.endEditing(true)
        scrollViewBottomConstraint.constant = 50
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
        return false
    }
}
