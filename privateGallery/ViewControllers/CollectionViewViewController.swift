import UIKit
import SwiftyKeychainKit

class CollectionViewViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!

    private let alertView = SampleView.instanceFromNib()
    private let service = Keychain(service: "Andrei-Shurykin.privateGallery")
    private var key = KeychainKey<String>(key: "1111")
    var photoObjectsArray: [Photo] = []
    private lazy var imagePicker = UIImagePickerController()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print(self,"deinitialized")
    }
    
    
    override func loadView() {
        super.loadView()
        print(self, "view loaded")
    }
        
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        alertView.mainTextField.delegate = self
        settingsButton.setTitle(settingsButtonTitle, for: .normal)
        addPhotoButton.setTitle(addPhotoButtonTitle, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let array = UserDefaults.standard.value([ImageAttributes].self, forKey: Keys.imageAttributes.rawValue) else {
            return
        }
        photoObjectsArray.removeAll()
        for item in array {
            let image = OpenImageFileManager.shared.openFileWithImage(fileName: item.imageFileURLName)
            let photo = Photo(name: item.imageFileURLName, photo: image, likeState: item.likeState, comment: item.comment)
            photoObjectsArray.append(photo)
        }
        collectionView.reloadData()
        viewWillLayoutSubviews()
        viewWillLayoutSubviews()
    }
    
    @IBAction func addPhotoButtonPressed(_ sender: UIButton) {
        let alertController = UIAlertController(title: alertLableText, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: alertGalleryButtonTitle, style: .default, handler: { (_) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            self.imagePicker.modalPresentationStyle = .fullScreen
        }))
        alertController.addAction(UIAlertAction(title: alertCameraButtonTitle, style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: alertCancelButtonTitle, style: .cancel, handler: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        view.addSubview(alertView)
        ViewManager.shared.setMainViewAttributes(view: alertView)
        alertView.backgroundColor = .white
        alertView.mainTextField.text = ""
        alertView.mainLabel.text = alertViewLableEnterNewText
        alertView.mainLabel.backgroundColor = .white
        alertView.additionalView.roundCorners()
        alertView.enterButton.setBorderAttributes()
        alertView.enterButton.backgroundColor = .white
        alertView.enterButton.setTitle(alertViewButtonTitle, for: .normal)
        alertView.enterButton.addTarget(self, action: #selector(enterButtonPressed(_sender:)), for: .touchUpInside)
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        alertView.addGestureRecognizer(swipeRecognizer)
    }
    
    @objc private func swipeDetected(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .right {
            alertView.removeFromSuperview()
        }
    }
    
    @objc private func enterButtonPressed(_sender: UIButton) {
        guard let password = alertView.mainTextField.text else {
            return
        }
        do {
            try service.set(password, for: key)
        } catch let err {
            print(err)
        }
        alertView.removeFromSuperview()
    }
}

extension CollectionViewViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            SaveImageManager.shared.saveImageToFile(currentImage: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoObjectsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.setupCell(imageObject: photoObjectsArray[indexPath.item])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = self.view.frame.size.width
        return CGSize(width: (screenWidth - minSpacingForCells * (imageInRow - 1)) / imageInRow, height: screenWidth / imageInRow)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else {
            return
        }
        controller.imageObject = photoObjectsArray[indexPath.row]
        controller.photoObjectsArray = photoObjectsArray
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        guard let password = alertView.mainTextField.text else {
                return false
            }
            do {
                try service.set(password, for: key)
            } catch let err {
                print(err)
            }
            alertView.removeFromSuperview()
        return false
    }
}
