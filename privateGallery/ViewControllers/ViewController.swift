import UIKit
import SwiftyKeychainKit

class ViewController: UIViewController {

    private let alertView = SampleView.instanceFromNib()
    private let service = Keychain(service: "Andrei-Shurykin.privateGallery")
    private var key = KeychainKey<String>(key: "1111")
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(alertView)
        setViewAttributes()
        conditionCheck()
    }
    
    @objc private func enterButtonPressed(_sender: UIButton) {
        checkPasswordAttributes()
    }
    
    private func setViewAttributes() {
        ViewManager.shared.setMainViewAttributes(view: alertView)
        alertView.additionalView.roundCorners()
        alertView.enterButton.setBorderAttributes()
        alertView.enterButton.setTitle(alertViewButtonTitle, for: .normal)
        alertView.enterButton.addTarget(self, action: #selector(enterButtonPressed(_sender:)), for: .touchUpInside)
        alertView.mainTextField.delegate = self
    }
    
    private func checkPasswordAttributes() {
        do {
            if let password = try service.get(key) {
                if password != "" {
                    if alertView.mainTextField.text == password {
                        goToNextViewController()
                    } else {
                        print("Error")
                    }
                } else {
                    guard let password = self.alertView.mainTextField.text else {
                        return
                    }
                    do {
                        try service.set(password, for: key)
                        goToNextViewController()
                    } catch let err {
                        print(err)
                    }
                }
            } else {
                guard let password = self.alertView.mainTextField.text else {
                    return
                }
                try service.set(password, for: key)
                goToNextViewController()
            }
        } catch let err {
            print(err)
        }
    }
    
    private func conditionCheck() {
        do {
            if let password = try service.get(key) {
                if password != "" {
                    alertView.mainLabel.text = alertViewLableEnterText
                } else {
                    alertView.mainLabel.text = alertViewLableSetText
                }
            }
            else {
                alertView.mainLabel.text = alertViewLableSetText
            }
        } catch let err {
            print(err)
        }
    }
    
    private func goToNextViewController() {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "CollectionViewViewController") as? CollectionViewViewController else {
            return
        }
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        checkPasswordAttributes()
        return false
    }

}
