import UIKit

class Photo {
    var name: String
    var photo: UIImage
    var likeState: Bool
    var comment: String
    
    internal init(name: String, photo: UIImage, likeState: Bool, comment: String) {
        self.name = name
        self.photo = photo
        self.likeState = likeState
        self.comment = comment
    }
}
