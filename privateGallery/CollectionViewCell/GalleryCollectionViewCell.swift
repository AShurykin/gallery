import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var GalleryCellView: UIView!
    @IBOutlet weak var customImageView: UIImageView!
    
    func setupCell(imageObject: Photo) {
        customImageView.image = imageObject.photo
    }
}
