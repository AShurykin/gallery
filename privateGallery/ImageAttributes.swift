import UIKit

class ImageAttributes: Codable {
    
    let imageFileURLName: String
    var likeState: Bool
    var comment: String
    
    internal init(imageFileURLName: String, likeState: Bool, comment: String) {
        self.imageFileURLName = imageFileURLName
        self.likeState = likeState
        self.comment = comment
    }
    
    public enum CodingKeys: String, CodingKey {
        case imageFileURLName, likeState, comment
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.imageFileURLName, forKey: .imageFileURLName)
        try container.encode(self.likeState, forKey: .likeState)
        try container.encode(self.comment, forKey: .comment)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.imageFileURLName = try (container.decodeIfPresent(String.self, forKey: .imageFileURLName) ?? "imageFileURLName")
        self.likeState = try (container.decodeIfPresent(Bool.self, forKey: .likeState) ?? false)
        self.comment = try (container.decodeIfPresent(String.self, forKey: .comment) ?? "comment")
    }
}
