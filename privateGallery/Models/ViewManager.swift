import UIKit

class ViewManager {
    
    static let shared = ViewManager()
    private init() {}
    
    func setMainViewAttributes(view: UIView) {
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        view.backgroundColor = .clear
    }
}
