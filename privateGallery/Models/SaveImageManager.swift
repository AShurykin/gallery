import UIKit

class SaveImageManager {
    
    static let shared = SaveImageManager()
    private init() {}
    
    func saveImageToFile(currentImage: UIImage?) {
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURLName = UUID().uuidString
            let customImage = ImageAttributes(imageFileURLName: fileURLName, likeState: false, comment: "")
            if var currentCustomImageArray = UserDefaults.standard.value([ImageAttributes].self, forKey: Keys.imageAttributes.rawValue) {
                currentCustomImageArray.append(customImage)
                UserDefaults.standard.set(encodable: currentCustomImageArray, forKey: Keys.imageAttributes.rawValue)
            } else {
                let currentCustomImageArray = [customImage]
                UserDefaults.standard.set(encodable: currentCustomImageArray, forKey: Keys.imageAttributes.rawValue)
            }
            let fileURL = documentDirectory.appendingPathComponent(fileURLName)
            let image = currentImage
            if let imageData = image?.jpegData(compressionQuality: 1) {
                try imageData.write(to: fileURL)
            }
        } catch {
            print(error)
        }
    }
}
