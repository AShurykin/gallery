import UIKit

class SwipeManager {
    static let shared = SwipeManager()
    private init() {}
    
    func leftSwipe(rightCurrentImageView: UIImageView,
                   galleryImageView: UIImageView,
                   myImagesCounter: Int,
                   myImagesNumber: Int,
                   myImages: [UIImage],
                   mainX: CGFloat) {
        var imageCounter = myImagesCounter - 1
        if imageCounter < 0 {
            imageCounter = myImagesNumber - 1
            rightCurrentImageView.image = myImages[imageCounter]
            UIView.animate(withDuration: 0.5) {
                rightCurrentImageView.frame.origin.x = mainX
            }
        } else {
            rightCurrentImageView.image = myImages[myImagesCounter]
            UIView.animate(withDuration: 0.5) {
                rightCurrentImageView.frame.origin.x = mainX
            }
        }
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in
            galleryImageView.image = rightCurrentImageView.image
            rightCurrentImageView.removeFromSuperview()
            }
    }
}
