import UIKit

class OpenImageFileManager {
    
    static let shared = OpenImageFileManager()
    private init() {}
    
    func openFileWithImage(fileName: String) -> UIImage {
                var currentImage = UIImage()
                let fileManager = FileManager.default
                do {
                    let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                    let fileURL = documentDirectory.appendingPathComponent(fileName)
                    let imageData = try Data(contentsOf: fileURL)
                    guard let image = UIImage(data: imageData) else {
                        return UIImage()
                    }
                    currentImage = image
                } catch {
                    print("Error loading image : \(error)")
                }
        return currentImage
    }
}
