import Foundation

let alertViewLableEnterText = "Enter password".locatized()
let alertViewLableSetText = "Set password".locatized()
let alertViewButtonTitle = "Enter".locatized()

let addPhotoButtonTitle = "+ Add photo".locatized()
let settingsButtonTitle = "Settings".locatized()
let alertViewLableEnterNewText = "Enter new password".locatized()
let alertLableText = "Add photo from:".locatized()
let alertGalleryButtonTitle = "Gallery".locatized()
let alertCameraButtonTitle = "Camera".locatized()
let alertCancelButtonTitle = "Cancel".locatized()

let backButtonTitle = "Back".locatized()
